# nCatcher

The nCatcher transforms an Arduino Nano to a proportional counter readout with pulse shape analysis - time over threshold measurement and a 10-bit analog-to-digital converter for pulse heights. The device is suitable for low to medium rate environments up to 5 kHz, where a good signal to noise ratio is crucial. The main development goal was the monitoring of thermal neutrons. 

## Getting started

Board:
Use KiCAD https://www.kicad.org/

Firmware:
Arduino Studio https://www.arduino.cc/en/software

## Contributors

Jannis Weimar, Physikalisches Institut, Heidelberg University


## Acknowledgment
We acknowledge the electronics workshop of the Physiklaisches Institut, Heidelberg University, for their steady support and helpful suggestions. We acknowledge Prof. Dr. Ulrich Schmidt, Heidelberg University, for supporting this project. We acknowledge StyX Neutronica for constant support of this project. 

## License
Board files and related documents are licence under CC BY-SA 4.0. Firmware code is licenced under GNU GPLv3.

## Project status
Readme Files to be added.
